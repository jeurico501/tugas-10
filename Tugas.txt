1.  Membuat database

    CREATE DATABASE myshop;

2.  Membuat Table di Dalam Database

    CREATE TABLE users ( id int PRIMARY KEY AUTO_INCREMENT, name VARCHAR(255) NOT null, email VARCHAR(255) NOT null, password VARCHAR(255) NOT null );

    CREATE TABLE categories ( id int PRIMARY KEY AUTO_INCREMENT, name VARCHAR(255) NOT null   );

    CREATE TABLE items ( id int PRIMARY KEY AUTO_INCREMENT, name VARCHAR(255) NOT null, description VARCHAR(255) NOT null, price int NOT null, stock int NOT null, category_id int NOT null, FOREIGN KEY(category_id) REFERENCES categories(id) );

3.  Memasukkan Data pada Table

    INSERT INTO users(name, email, password) VALUES( "John Doe","john@doe.com","john123" ), ( "Jane Doe", "jane@doe.com", "jenita123" );

INSERT INTO categories(name) VALUES("gadget"), ("cloth"), ("men"), ("women"), ("branded");

INSERT INTO items(name, description, price, stock, category_id) VALUES( "Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1 ), ( "Uniklooh", "baju keren dari brand ternama", 500000, 50, 2 ), ( "IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1 );

4. Mengambil Data dari Database

    a). Mengambil data users

        SELECT name, email FROM users;

    b). Mengambil data items

        - Membuat Query untuk mengambil data item pada table items yang memiliki data di atas 100000
            SELECT name, email FROM users;

        - Membuat Query untuk mengambil data item pada table items yang memiliki name like keywords: “uniklo”, “watch”, atau “sang”
            SELECT * FROM items WHERE name LIKE “watch%";

    c). Menampilkan data items join dengan kategori

        SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name AS kategori from items INNER JOIN categories ON items.category_id = categories.id;

5. Mengubah Data dari Database

    UPDATE items SET price = 2500000 WHERE name = 'sumsang b50';